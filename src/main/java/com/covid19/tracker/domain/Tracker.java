package com.covid19.tracker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Tracker.
 */
@Entity
@Table(name = "tracker")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Tracker implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "consulted_at")
    private ZonedDateTime consultedAt;

    @Column(name = "consulted_by")
    private String consultedBy;

    @OneToMany(mappedBy = "tracker")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<TrackerState> trackerStates = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getConsultedAt() {
        return consultedAt;
    }

    public Tracker consultedAt(ZonedDateTime consultedAt) {
        this.consultedAt = consultedAt;
        return this;
    }

    public void setConsultedAt(ZonedDateTime consultedAt) {
        this.consultedAt = consultedAt;
    }

    public String getConsultedBy() {
        return consultedBy;
    }

    public Tracker consultedBy(String consultedBy) {
        this.consultedBy = consultedBy;
        return this;
    }

    public void setConsultedBy(String consultedBy) {
        this.consultedBy = consultedBy;
    }

    public Set<TrackerState> getTrackerStates() {
        return trackerStates;
    }

    public Tracker trackerStates(Set<TrackerState> trackerStates) {
        this.trackerStates = trackerStates;
        return this;
    }

    public Tracker addTrackerStates(TrackerState trackerState) {
        this.trackerStates.add(trackerState);
        trackerState.setTracker(this);
        return this;
    }

    public Tracker removeTrackerStates(TrackerState trackerState) {
        this.trackerStates.remove(trackerState);
        trackerState.setTracker(null);
        return this;
    }

    public void setTrackerStates(Set<TrackerState> trackerStates) {
        this.trackerStates = trackerStates;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tracker)) {
            return false;
        }
        return id != null && id.equals(((Tracker) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Tracker{" +
            "id=" + getId() +
            ", consultedAt='" + getConsultedAt() + "'" +
            ", consultedBy='" + getConsultedBy() + "'" +
            "}";
    }
}
