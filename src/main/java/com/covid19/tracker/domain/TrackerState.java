package com.covid19.tracker.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A TrackerState.
 */
@Entity
@Table(name = "tracker_state")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TrackerState implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "phase")
    private String phase;

    @Column(name = "total_vaccines")
    private Integer totalVaccines;

    @Column(name = "phase_detail")
    private String phaseDetail;

    @ManyToOne
    @JsonIgnoreProperties(value = "trackerStates", allowSetters = true)
    private Tracker tracker;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhase() {
        return phase;
    }

    public TrackerState phase(String phase) {
        this.phase = phase;
        return this;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public Integer getTotalVaccines() {
        return totalVaccines;
    }

    public TrackerState totalVaccines(Integer totalVaccines) {
        this.totalVaccines = totalVaccines;
        return this;
    }

    public void setTotalVaccines(Integer totalVaccines) {
        this.totalVaccines = totalVaccines;
    }

    public String getPhaseDetail() {
        return phaseDetail;
    }

    public TrackerState phaseDetail(String phaseDetail) {
        this.phaseDetail = phaseDetail;
        return this;
    }

    public void setPhaseDetail(String phaseDetail) {
        this.phaseDetail = phaseDetail;
    }

    public Tracker getTracker() {
        return tracker;
    }

    public TrackerState tracker(Tracker tracker) {
        this.tracker = tracker;
        return this;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TrackerState)) {
            return false;
        }
        return id != null && id.equals(((TrackerState) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TrackerState{" +
            "id=" + getId() +
            ", phase='" + getPhase() + "'" +
            ", totalVaccines=" + getTotalVaccines() +
            ", phaseDetail='" + getPhaseDetail() + "'" +
            "}";
    }
}
