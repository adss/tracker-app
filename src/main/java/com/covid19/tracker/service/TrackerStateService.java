package com.covid19.tracker.service;

import com.covid19.tracker.service.dto.TrackerStateDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.covid19.tracker.domain.TrackerState}.
 */
public interface TrackerStateService {

    /**
     * Save a trackerState.
     *
     * @param trackerStateDTO the entity to save.
     * @return the persisted entity.
     */
    TrackerStateDTO save(TrackerStateDTO trackerStateDTO);

    /**
     * Get all the trackerStates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TrackerStateDTO> findAll(Pageable pageable);


    /**
     * Get the "id" trackerState.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TrackerStateDTO> findOne(Long id);

    /**
     * Delete the "id" trackerState.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
