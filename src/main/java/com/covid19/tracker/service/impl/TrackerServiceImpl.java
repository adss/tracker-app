package com.covid19.tracker.service.impl;

import com.covid19.tracker.service.TrackerService;
import com.covid19.tracker.domain.Tracker;
import com.covid19.tracker.repository.TrackerRepository;
import com.covid19.tracker.service.dto.TrackerDTO;
import com.covid19.tracker.service.mapper.TrackerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Tracker}.
 */
@Service
@Transactional
public class TrackerServiceImpl implements TrackerService {

    private final Logger log = LoggerFactory.getLogger(TrackerServiceImpl.class);

    private final TrackerRepository trackerRepository;

    private final TrackerMapper trackerMapper;

    public TrackerServiceImpl(TrackerRepository trackerRepository, TrackerMapper trackerMapper) {
        this.trackerRepository = trackerRepository;
        this.trackerMapper = trackerMapper;
    }

    @Override
    public TrackerDTO save(TrackerDTO trackerDTO) {
        log.debug("Request to save Tracker : {}", trackerDTO);
        Tracker tracker = trackerMapper.toEntity(trackerDTO);
        tracker = trackerRepository.save(tracker);
        return trackerMapper.toDto(tracker);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TrackerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Trackers");
        return trackerRepository.findAll(pageable)
            .map(trackerMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<TrackerDTO> findOne(Long id) {
        log.debug("Request to get Tracker : {}", id);
        return trackerRepository.findById(id)
            .map(trackerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Tracker : {}", id);
        trackerRepository.deleteById(id);
    }
}
