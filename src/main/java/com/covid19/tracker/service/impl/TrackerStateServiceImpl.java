package com.covid19.tracker.service.impl;

import com.covid19.tracker.service.TrackerStateService;
import com.covid19.tracker.domain.TrackerState;
import com.covid19.tracker.repository.TrackerStateRepository;
import com.covid19.tracker.service.dto.TrackerStateDTO;
import com.covid19.tracker.service.mapper.TrackerStateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link TrackerState}.
 */
@Service
@Transactional
public class TrackerStateServiceImpl implements TrackerStateService {

    private final Logger log = LoggerFactory.getLogger(TrackerStateServiceImpl.class);

    private final TrackerStateRepository trackerStateRepository;

    private final TrackerStateMapper trackerStateMapper;

    public TrackerStateServiceImpl(TrackerStateRepository trackerStateRepository, TrackerStateMapper trackerStateMapper) {
        this.trackerStateRepository = trackerStateRepository;
        this.trackerStateMapper = trackerStateMapper;
    }

    @Override
    public TrackerStateDTO save(TrackerStateDTO trackerStateDTO) {
        log.debug("Request to save TrackerState : {}", trackerStateDTO);
        TrackerState trackerState = trackerStateMapper.toEntity(trackerStateDTO);
        trackerState = trackerStateRepository.save(trackerState);
        return trackerStateMapper.toDto(trackerState);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TrackerStateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TrackerStates");
        return trackerStateRepository.findAll(pageable)
            .map(trackerStateMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<TrackerStateDTO> findOne(Long id) {
        log.debug("Request to get TrackerState : {}", id);
        return trackerStateRepository.findById(id)
            .map(trackerStateMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TrackerState : {}", id);
        trackerStateRepository.deleteById(id);
    }
}
