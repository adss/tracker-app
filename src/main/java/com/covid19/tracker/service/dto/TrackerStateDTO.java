package com.covid19.tracker.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.covid19.tracker.domain.TrackerState} entity.
 */
public class TrackerStateDTO implements Serializable {
    
    private Long id;

    private String phase;

    private Integer totalVaccines;

    private String phaseDetail;


    private Long trackerId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public Integer getTotalVaccines() {
        return totalVaccines;
    }

    public void setTotalVaccines(Integer totalVaccines) {
        this.totalVaccines = totalVaccines;
    }

    public String getPhaseDetail() {
        return phaseDetail;
    }

    public void setPhaseDetail(String phaseDetail) {
        this.phaseDetail = phaseDetail;
    }

    public Long getTrackerId() {
        return trackerId;
    }

    public void setTrackerId(Long trackerId) {
        this.trackerId = trackerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TrackerStateDTO)) {
            return false;
        }

        return id != null && id.equals(((TrackerStateDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TrackerStateDTO{" +
            "id=" + getId() +
            ", phase='" + getPhase() + "'" +
            ", totalVaccines=" + getTotalVaccines() +
            ", phaseDetail='" + getPhaseDetail() + "'" +
            ", trackerId=" + getTrackerId() +
            "}";
    }
}
