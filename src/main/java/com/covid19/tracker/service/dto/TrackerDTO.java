package com.covid19.tracker.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;

/**
 * A DTO for the {@link com.covid19.tracker.domain.Tracker} entity.
 */
public class TrackerDTO implements Serializable {
    
    private Long id;

    private ZonedDateTime consultedAt;

    private String consultedBy;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getConsultedAt() {
        return consultedAt;
    }

    public void setConsultedAt(ZonedDateTime consultedAt) {
        this.consultedAt = consultedAt;
    }

    public String getConsultedBy() {
        return consultedBy;
    }

    public void setConsultedBy(String consultedBy) {
        this.consultedBy = consultedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TrackerDTO)) {
            return false;
        }

        return id != null && id.equals(((TrackerDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TrackerDTO{" +
            "id=" + getId() +
            ", consultedAt='" + getConsultedAt() + "'" +
            ", consultedBy='" + getConsultedBy() + "'" +
            "}";
    }
}
