package com.covid19.tracker.service.mapper;


import com.covid19.tracker.domain.*;
import com.covid19.tracker.service.dto.TrackerStateDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TrackerState} and its DTO {@link TrackerStateDTO}.
 */
@Mapper(componentModel = "spring", uses = {TrackerMapper.class})
public interface TrackerStateMapper extends EntityMapper<TrackerStateDTO, TrackerState> {

    @Mapping(source = "tracker.id", target = "trackerId")
    TrackerStateDTO toDto(TrackerState trackerState);

    @Mapping(source = "trackerId", target = "tracker")
    TrackerState toEntity(TrackerStateDTO trackerStateDTO);

    default TrackerState fromId(Long id) {
        if (id == null) {
            return null;
        }
        TrackerState trackerState = new TrackerState();
        trackerState.setId(id);
        return trackerState;
    }
}
