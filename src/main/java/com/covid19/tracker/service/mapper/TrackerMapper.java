package com.covid19.tracker.service.mapper;


import com.covid19.tracker.domain.*;
import com.covid19.tracker.service.dto.TrackerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Tracker} and its DTO {@link TrackerDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TrackerMapper extends EntityMapper<TrackerDTO, Tracker> {


    @Mapping(target = "trackerStates", ignore = true)
    @Mapping(target = "removeTrackerStates", ignore = true)
    Tracker toEntity(TrackerDTO trackerDTO);

    default Tracker fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tracker tracker = new Tracker();
        tracker.setId(id);
        return tracker;
    }
}
