package com.covid19.tracker.repository;

import com.covid19.tracker.domain.TrackerState;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the TrackerState entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrackerStateRepository extends JpaRepository<TrackerState, Long> {
}
