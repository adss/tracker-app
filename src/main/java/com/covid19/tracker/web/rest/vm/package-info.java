/**
 * View Models used by Spring MVC REST controllers.
 */
package com.covid19.tracker.web.rest.vm;
