package com.covid19.tracker.web.rest;

import com.covid19.tracker.service.TrackerService;
import com.covid19.tracker.web.rest.errors.BadRequestAlertException;
import com.covid19.tracker.service.dto.TrackerDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.covid19.tracker.domain.Tracker}.
 */
@RestController
@RequestMapping("/api")
public class TrackerResource {

    private final Logger log = LoggerFactory.getLogger(TrackerResource.class);

    private final TrackerService trackerService;

    public TrackerResource(TrackerService trackerService) {
        this.trackerService = trackerService;
    }

    /**
     * {@code GET  /trackers} : get all the trackers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of trackers in body.
     */
    @GetMapping("/trackers")
    public ResponseEntity<List<TrackerDTO>> getAllTrackers(Pageable pageable) {
        log.debug("REST request to get a page of Trackers");
        Page<TrackerDTO> page = trackerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /trackers/:id} : get the "id" tracker.
     *
     * @param id the id of the trackerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the trackerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/trackers/{id}")
    public ResponseEntity<TrackerDTO> getTracker(@PathVariable Long id) {
        log.debug("REST request to get Tracker : {}", id);
        Optional<TrackerDTO> trackerDTO = trackerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(trackerDTO);
    }
}
