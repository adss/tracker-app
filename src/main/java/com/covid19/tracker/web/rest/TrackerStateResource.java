package com.covid19.tracker.web.rest;

import com.covid19.tracker.service.TrackerStateService;
import com.covid19.tracker.web.rest.errors.BadRequestAlertException;
import com.covid19.tracker.service.dto.TrackerStateDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.covid19.tracker.domain.TrackerState}.
 */
@RestController
@RequestMapping("/api")
public class TrackerStateResource {

    private final Logger log = LoggerFactory.getLogger(TrackerStateResource.class);

    private final TrackerStateService trackerStateService;

    public TrackerStateResource(TrackerStateService trackerStateService) {
        this.trackerStateService = trackerStateService;
    }

    /**
     * {@code GET  /tracker-states} : get all the trackerStates.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of trackerStates in body.
     */
    @GetMapping("/tracker-states")
    public ResponseEntity<List<TrackerStateDTO>> getAllTrackerStates(Pageable pageable) {
        log.debug("REST request to get a page of TrackerStates");
        Page<TrackerStateDTO> page = trackerStateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tracker-states/:id} : get the "id" trackerState.
     *
     * @param id the id of the trackerStateDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the trackerStateDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tracker-states/{id}")
    public ResponseEntity<TrackerStateDTO> getTrackerState(@PathVariable Long id) {
        log.debug("REST request to get TrackerState : {}", id);
        Optional<TrackerStateDTO> trackerStateDTO = trackerStateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(trackerStateDTO);
    }
}
