package com.covid19.tracker.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.covid19.tracker.web.rest.TestUtil;

public class TrackerDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrackerDTO.class);
        TrackerDTO trackerDTO1 = new TrackerDTO();
        trackerDTO1.setId(1L);
        TrackerDTO trackerDTO2 = new TrackerDTO();
        assertThat(trackerDTO1).isNotEqualTo(trackerDTO2);
        trackerDTO2.setId(trackerDTO1.getId());
        assertThat(trackerDTO1).isEqualTo(trackerDTO2);
        trackerDTO2.setId(2L);
        assertThat(trackerDTO1).isNotEqualTo(trackerDTO2);
        trackerDTO1.setId(null);
        assertThat(trackerDTO1).isNotEqualTo(trackerDTO2);
    }
}
