package com.covid19.tracker.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.covid19.tracker.web.rest.TestUtil;

public class TrackerStateDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrackerStateDTO.class);
        TrackerStateDTO trackerStateDTO1 = new TrackerStateDTO();
        trackerStateDTO1.setId(1L);
        TrackerStateDTO trackerStateDTO2 = new TrackerStateDTO();
        assertThat(trackerStateDTO1).isNotEqualTo(trackerStateDTO2);
        trackerStateDTO2.setId(trackerStateDTO1.getId());
        assertThat(trackerStateDTO1).isEqualTo(trackerStateDTO2);
        trackerStateDTO2.setId(2L);
        assertThat(trackerStateDTO1).isNotEqualTo(trackerStateDTO2);
        trackerStateDTO1.setId(null);
        assertThat(trackerStateDTO1).isNotEqualTo(trackerStateDTO2);
    }
}
