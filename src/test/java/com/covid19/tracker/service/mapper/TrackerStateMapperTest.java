package com.covid19.tracker.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TrackerStateMapperTest {

    private TrackerStateMapper trackerStateMapper;

    @BeforeEach
    public void setUp() {
        trackerStateMapper = new TrackerStateMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(trackerStateMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(trackerStateMapper.fromId(null)).isNull();
    }
}
