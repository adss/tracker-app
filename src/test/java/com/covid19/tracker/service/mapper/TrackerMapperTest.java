package com.covid19.tracker.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TrackerMapperTest {

    private TrackerMapper trackerMapper;

    @BeforeEach
    public void setUp() {
        trackerMapper = new TrackerMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(trackerMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(trackerMapper.fromId(null)).isNull();
    }
}
