package com.covid19.tracker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.covid19.tracker.web.rest.TestUtil;

public class TrackerStateTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrackerState.class);
        TrackerState trackerState1 = new TrackerState();
        trackerState1.setId(1L);
        TrackerState trackerState2 = new TrackerState();
        trackerState2.setId(trackerState1.getId());
        assertThat(trackerState1).isEqualTo(trackerState2);
        trackerState2.setId(2L);
        assertThat(trackerState1).isNotEqualTo(trackerState2);
        trackerState1.setId(null);
        assertThat(trackerState1).isNotEqualTo(trackerState2);
    }
}
