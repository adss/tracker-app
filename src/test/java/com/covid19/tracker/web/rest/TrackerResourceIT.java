package com.covid19.tracker.web.rest;

import com.covid19.tracker.TrackerApp;
import com.covid19.tracker.domain.Tracker;
import com.covid19.tracker.repository.TrackerRepository;
import com.covid19.tracker.service.TrackerService;
import com.covid19.tracker.service.dto.TrackerDTO;
import com.covid19.tracker.service.mapper.TrackerMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.covid19.tracker.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TrackerResource} REST controller.
 */
@SpringBootTest(classes = TrackerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class TrackerResourceIT {

    private static final ZonedDateTime DEFAULT_CONSULTED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CONSULTED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CONSULTED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CONSULTED_BY = "BBBBBBBBBB";

    @Autowired
    private TrackerRepository trackerRepository;

    @Autowired
    private TrackerMapper trackerMapper;

    @Autowired
    private TrackerService trackerService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTrackerMockMvc;

    private Tracker tracker;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tracker createEntity(EntityManager em) {
        Tracker tracker = new Tracker()
            .consultedAt(DEFAULT_CONSULTED_AT)
            .consultedBy(DEFAULT_CONSULTED_BY);
        return tracker;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tracker createUpdatedEntity(EntityManager em) {
        Tracker tracker = new Tracker()
            .consultedAt(UPDATED_CONSULTED_AT)
            .consultedBy(UPDATED_CONSULTED_BY);
        return tracker;
    }

    @BeforeEach
    public void initTest() {
        tracker = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllTrackers() throws Exception {
        // Initialize the database
        trackerRepository.saveAndFlush(tracker);

        // Get all the trackerList
        restTrackerMockMvc.perform(get("/api/trackers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tracker.getId().intValue())))
            .andExpect(jsonPath("$.[*].consultedAt").value(hasItem(sameInstant(DEFAULT_CONSULTED_AT))))
            .andExpect(jsonPath("$.[*].consultedBy").value(hasItem(DEFAULT_CONSULTED_BY)));
    }
    
    @Test
    @Transactional
    public void getTracker() throws Exception {
        // Initialize the database
        trackerRepository.saveAndFlush(tracker);

        // Get the tracker
        restTrackerMockMvc.perform(get("/api/trackers/{id}", tracker.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tracker.getId().intValue()))
            .andExpect(jsonPath("$.consultedAt").value(sameInstant(DEFAULT_CONSULTED_AT)))
            .andExpect(jsonPath("$.consultedBy").value(DEFAULT_CONSULTED_BY));
    }
    @Test
    @Transactional
    public void getNonExistingTracker() throws Exception {
        // Get the tracker
        restTrackerMockMvc.perform(get("/api/trackers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
