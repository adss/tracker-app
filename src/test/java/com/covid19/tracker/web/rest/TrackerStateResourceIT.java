package com.covid19.tracker.web.rest;

import com.covid19.tracker.TrackerApp;
import com.covid19.tracker.domain.TrackerState;
import com.covid19.tracker.repository.TrackerStateRepository;
import com.covid19.tracker.service.TrackerStateService;
import com.covid19.tracker.service.dto.TrackerStateDTO;
import com.covid19.tracker.service.mapper.TrackerStateMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TrackerStateResource} REST controller.
 */
@SpringBootTest(classes = TrackerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class TrackerStateResourceIT {

    private static final String DEFAULT_PHASE = "AAAAAAAAAA";
    private static final String UPDATED_PHASE = "BBBBBBBBBB";

    private static final Integer DEFAULT_TOTAL_VACCINES = 1;
    private static final Integer UPDATED_TOTAL_VACCINES = 2;

    private static final String DEFAULT_PHASE_DETAIL = "AAAAAAAAAA";
    private static final String UPDATED_PHASE_DETAIL = "BBBBBBBBBB";

    @Autowired
    private TrackerStateRepository trackerStateRepository;

    @Autowired
    private TrackerStateMapper trackerStateMapper;

    @Autowired
    private TrackerStateService trackerStateService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTrackerStateMockMvc;

    private TrackerState trackerState;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrackerState createEntity(EntityManager em) {
        TrackerState trackerState = new TrackerState()
            .phase(DEFAULT_PHASE)
            .totalVaccines(DEFAULT_TOTAL_VACCINES)
            .phaseDetail(DEFAULT_PHASE_DETAIL);
        return trackerState;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrackerState createUpdatedEntity(EntityManager em) {
        TrackerState trackerState = new TrackerState()
            .phase(UPDATED_PHASE)
            .totalVaccines(UPDATED_TOTAL_VACCINES)
            .phaseDetail(UPDATED_PHASE_DETAIL);
        return trackerState;
    }

    @BeforeEach
    public void initTest() {
        trackerState = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllTrackerStates() throws Exception {
        // Initialize the database
        trackerStateRepository.saveAndFlush(trackerState);

        // Get all the trackerStateList
        restTrackerStateMockMvc.perform(get("/api/tracker-states?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trackerState.getId().intValue())))
            .andExpect(jsonPath("$.[*].phase").value(hasItem(DEFAULT_PHASE)))
            .andExpect(jsonPath("$.[*].totalVaccines").value(hasItem(DEFAULT_TOTAL_VACCINES)))
            .andExpect(jsonPath("$.[*].phaseDetail").value(hasItem(DEFAULT_PHASE_DETAIL)));
    }
    
    @Test
    @Transactional
    public void getTrackerState() throws Exception {
        // Initialize the database
        trackerStateRepository.saveAndFlush(trackerState);

        // Get the trackerState
        restTrackerStateMockMvc.perform(get("/api/tracker-states/{id}", trackerState.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(trackerState.getId().intValue()))
            .andExpect(jsonPath("$.phase").value(DEFAULT_PHASE))
            .andExpect(jsonPath("$.totalVaccines").value(DEFAULT_TOTAL_VACCINES))
            .andExpect(jsonPath("$.phaseDetail").value(DEFAULT_PHASE_DETAIL));
    }
    @Test
    @Transactional
    public void getNonExistingTrackerState() throws Exception {
        // Get the trackerState
        restTrackerStateMockMvc.perform(get("/api/tracker-states/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
